import Vue from 'vue'
import Router from 'vue-router'

import loginView from './views/Login.vue'
import homeView from './views/Home.vue'
import panelView from './views/Panel.vue'
import aboutView from './views/About.vue'
import detailView from './views/DetailAd.vue'
import dashboardMun from './views/DashboardMun.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: homeView
    },
    {
      path: '/login',
      name: 'login',
      component: loginView
    },
    {
      path: '/panel',
      name: 'panel',
      component: panelView
    },
    {
      path: '/about',
      name: 'about',
      component: aboutView
    },
    {
      path: '/details',
      name: 'details',
      component: detailView
    },
    {
      path: '/dashboard',
      name: 'dashboardMun',
      component: dashboardMun
    }
    
    
  ]
})